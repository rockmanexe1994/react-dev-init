# react-dev-init

A very simple module that help you bring up a React project quickly. It will save you a little time creating and configuring a new React project.

## Include
* react and react-dom
* babel and some of it's plugins and presets (for transpiling ES6 and JSX element)
* webpack
* webpack-dev-server (for live reloading)

## Installation
```
npm install -g react-dev-init
```

## Usage
Start a new project with npm normally...
```
npm init
```
... and run this command at the project root directory.
```
react-dev-init
```
After that, use this command to install all depedencies...
```
npm run install
```
...and start the project with
```
npm run dev
```
Then navigate to <b>http://localhost:8080</b>.
If you see, <b>"Yay! We're good to go now"</b>, then everything's good. You can start develop your features now.
