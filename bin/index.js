#!/usr/bin/env node
const path = require("path");
const fs = require("fs");

var currentPath = process.cwd();

//Create src directory
fs.mkdirSync(currentPath+"/src");
fs.mkdirSync(currentPath+"/src/js");

// Update pckage.json
console.log("Updating package.json...");
var packageJson = JSON.parse(fs.readFileSync(currentPath+'/package.json','utf-8'));
packageJson.scripts = {
    "build": "webpack",
    "dev":"webpack-dev-server --content-base src --inline --hot",
    "test": "echo \"Error: no test specified\" && exit 1",
    "install": "npm i -S react react-dom babel-core babel-loader babel-plugin-add-module-exports babel-plugin-react-html-attrs babel-plugin-transform-class-properties babel-plugin-transform-decorators-legacy babel-preset-es2015 babel-preset-react babel-preset-stage-0 webpack webpack-dev-server"
}
packageJson.main = "webpack.config.js"
fs.writeFileSync(currentPath+'/package.json',JSON.stringify(packageJson, null, 4),{encoding:'utf-8'});

// Create default webpack.config.js
console.log("Creating default webpack.config.js...");
var defaultImport = "var debug = process.env.NODE_ENV !== \"production\";\n"
+"var webpack = require(\'webpack\');\n\n"
+"module.exports = "

var defaultExports = "{\n"+
"    context: __dirname+\"/src\",\n"+
"    devtool: debug ? \"inline-sourcemap\": null,\n"+
"    entry: \"./js/client.js\",\n"+
"    module: {\n"+
"        loaders: [\n"+
"            {\n"+
"                test: /\\.js?$/,\n"+
"                exclude: /(node_modules|bower_components)/,\n"+
"                loader: \'babel-loader\',\n"+
"                query: {\n"+
"                    presets: [\'react\', \'es2015\', \'stage-0\'],\n"+
"                    plugins: [\'react-html-attrs\',\'transform-class-properties\',\'transform-decorators-legacy\']\n"+
"                }\n"+
"            }\n"+
"        ]\n"+
"    },\n"+
"    output:{\n"+
"        path: __dirname + \"/src/\",\n"+
"        filename: \"client.min.js\"\n"+
"    },\n"+
"    plugins: debug ? [] : [\n"+
"        new webpack.optimize.DedupePlugin(),\n"+
"        new webpack.optimize.OccurenceOrderPlugin(),\n"+
"        new webpack.optimize.UglifyJsPlugin({mangle: false, sourcemap: false}),\n"+
"    ]\n"+
"}";

var defaultWebPackJsonString = defaultImport + defaultExports;

fs.writeFileSync(currentPath+'/webpack.config.js',defaultWebPackJsonString,{encoding:'utf-8'});

// Create index.html
console.log("Creating default index.html...");
var defaultHTML = "<!DOCTYPE html>\n"+
"<html>\n"+
"    <head>\n"+
"        <meta charset=\"utf-8\"/>\n"+
"        <title>Welcome to React!</title>\n"+
"    <head>\n"+
"    <body>\n"+
"        <div id=\"app\"></div>\n"+
"        <script src=\"client.min.js\"></script>\n"+
"    <body>\n"+
"</html>\n"
fs.writeFileSync(currentPath+'/src/index.html',defaultHTML,{encoding:'utf-8'});

// Create client.js
console.log("Creating default client.js...");
var defaultClientJs = "import React from \"react\";\n"+
"import ReactDOM from \"react-dom\";\n\n"+
"class Layout extends React.Component {\n"+
"    render(){\n"+
"        return (\n"+
"            <h1>Yay! We're good to go now</h1>\n"+
"        )\n"+
"    }\n"+
"}\n\n"+
"const app = document.getElementById('app');\n"+
"ReactDOM.render(<Layout/>, app);"
fs.writeFileSync(currentPath+'/src/js/client.js',defaultClientJs,{encoding:'utf-8'});
